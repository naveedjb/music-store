﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(musicstore3.Startup))]
namespace musicstore3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
